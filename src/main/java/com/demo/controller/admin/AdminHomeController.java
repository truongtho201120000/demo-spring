package com.demo.controller.admin;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.service.ICategoryService;
import com.demo.service.IProductService;

@Controller
public class AdminHomeController {
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private IProductService productService;
	
	@RequestMapping(value = { "/admin/home" }, method = RequestMethod.GET)
	public String home(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		return "admin/index";
	}
	@RequestMapping(value = { "/admin/categories" }, method = RequestMethod.GET)
	public String listCategories(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		return "admin/categories";
	}
	@RequestMapping(value = { "/admin/add-category" }, method = RequestMethod.GET)
	public String addCategories(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		return "admin/add-category";
	}
	@RequestMapping(value = { "/admin/list-product" }, method = RequestMethod.GET)
	public String listProduct(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		model.addAttribute("listProduct", productService.findAll());
		return "admin/list-product";
	}
	@RequestMapping(value = { "/admin/add-product" }, method = RequestMethod.GET)
	public String addProduct(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		return "admin/add-product";
	}
}
