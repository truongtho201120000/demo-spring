package com.demo.controller.user;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HomeController {

	@RequestMapping(value = { "/home" }, method = RequestMethod.GET)
	@GetMapping("/home")
	public String home(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		return "user/home";
	}
}

//@RestController
//public class HomeController {
//
//	@GetMapping("/home")
//	public String home(final Model model, final HttpServletRequest request, final HttpServletResponse response)
//			throws IOException {
//		return "user/home";
//	}
//}
