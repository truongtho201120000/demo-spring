package com.demo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "tblCategory")
public class CategoryEntity extends BaseEntity{
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name="description",length = 1000, nullable = false)
	private String description;
	
	@Column(name="seo", nullable = true)
	private String seo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSeo() {
		return seo;
	}

	public void setSeo(String seo) {
		this.seo = seo;
	}
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "categories")
	public Set<ProductEntity> products = new HashSet<ProductEntity>();
	
	/**
	 * them  1 san pham vao danh sach @OneToMany
	 * @param product
	 */
	public void addRelationProduct(ProductEntity product) {
		products.add(product);
		product.setCategories(this);
	}

	/**
	 * xoa san pham khoi danh sach @OneToMany
	 * @param product
	 */
	public void deleteRelationProduct(ProductEntity product) {
		products.remove(product);
		product.setCategories(null);
	}
	

	public Set<ProductEntity> getProducts() {
		return products;
	}

	public void setProducts(Set<ProductEntity> products) {
		this.products = products;
	}
	
}
