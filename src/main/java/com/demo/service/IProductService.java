package com.demo.service;

import java.util.List;

import com.demo.entity.ProductEntity;

public interface IProductService{
	
	//Lay het
	public List<ProductEntity> findAll();
	
	//Lay 1
	public ProductEntity findOneProduct(long id);
	
	//Them
	public ProductEntity saveProduct(ProductEntity productEntity);
	
	//Sua
	public ProductEntity editProduct(ProductEntity productEntity, long id);
	
	//Xoa
	public void deleteProduct(long id);
	
	public List<ProductEntity> findByTitle(String title);
}
