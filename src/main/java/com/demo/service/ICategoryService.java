package com.demo.service;

import java.util.List;

import com.demo.entity.CategoryEntity;

public interface ICategoryService {
	//Lay het
	public List<CategoryEntity> findAll();
	
	//Lay 1
	public CategoryEntity findById(long id);
	
	//Them moi
	public CategoryEntity addCategory(CategoryEntity categoryEntity);
	
	//Luu
	public CategoryEntity saveCategory(CategoryEntity categoryEntity);
	
	//Sua
	public CategoryEntity editCategory(CategoryEntity categoryEntity, long id);
	
	//Xoa
	public void deleteCategory(long id);
	
}
