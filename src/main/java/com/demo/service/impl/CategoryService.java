package com.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.CategoryEntity;
import com.demo.repository.CategoryRepository;
import com.demo.service.ICategoryService;

@Service
public class CategoryService implements ICategoryService{
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@Override
	public List<CategoryEntity> findAll() {
		// TODO Auto-generated method stub
		return categoryRepository.findAll();
	}

	@Override
	public CategoryEntity findById(long id) {
		// TODO Auto-generated method stub
		return categoryRepository.findById(id).get();
	}

	@Override
	public CategoryEntity addCategory(CategoryEntity categoryEntity) {
		// TODO Auto-generated method stub
		return categoryRepository.save(categoryEntity);
	}

	@Override
	public CategoryEntity editCategory(CategoryEntity categoryEntity, long id) {
		// TODO Auto-generated method stub
		CategoryEntity cateFound = categoryRepository.findById(id).get();
		if(cateFound != null) {
			cateFound.setName(categoryEntity.getName());
			cateFound.setDescription(categoryEntity.getDescription());
			return categoryRepository.save(cateFound);
		}
		return null;
	}

	@Override
	public void deleteCategory(long id) {
		// TODO Auto-generated method stub
		categoryRepository.deleteById(id);
	}

	@Override
	public CategoryEntity saveCategory(CategoryEntity categoryEntity) {
		// TODO Auto-generated method stub
		return categoryRepository.save(categoryEntity);
	}

}
