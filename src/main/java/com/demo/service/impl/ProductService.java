package com.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.entity.ProductEntity;
import com.demo.repository.CategoryRepository;
import com.demo.repository.ProductRepository;
import com.demo.service.IProductService;

@Service
public class ProductService implements IProductService{
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<ProductEntity> findAll() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public ProductEntity findOneProduct(long id) {
		// TODO Auto-generated method stub
		return productRepository.findById(id).get();
	}

	@Override
	public ProductEntity saveProduct(ProductEntity productEntity) {
		return productRepository.save(productEntity);
	}

	@Override
	public ProductEntity editProduct(ProductEntity productEntity, long id) {
		// TODO Auto-generated method stub
		return productRepository.save(productEntity);
	}

	@Override
	public void deleteProduct(long id) {
		// TODO Auto-generated method stub
		productRepository.deleteById(id);
	}

	@Override
	public List<ProductEntity> findByTitle(String title) {
		// TODO Auto-generated method stub
		return productRepository.findByTitle(title);
	}

}
