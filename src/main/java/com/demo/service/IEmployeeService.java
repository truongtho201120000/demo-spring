package com.demo.service;

import java.util.List;
import java.util.Optional;

import com.demo.entity.Employee;

public interface IEmployeeService {
	//Lay tat ca
	public List<Employee> findAll();
	
	//Lay 1
	public Employee findOneEmployee(long id);
	
	//Tao moi
	public Employee saveEmployee(Employee employee);
	
	//Chinh sua
	public Employee editEmployee(long id, Employee employee);
	
	//Xoa
	public boolean deleteEmployee(long id);
	
	public boolean existById(long id);
	
	public List<Employee> findByEmployeeName(String name);
}
