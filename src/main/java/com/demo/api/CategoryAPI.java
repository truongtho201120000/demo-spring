package com.demo.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.ResponseObjectDTO;
import com.demo.entity.CategoryEntity;
import com.demo.entity.Employee;
import com.demo.service.ICategoryService;

@RestController
public class CategoryAPI {
	@Autowired
	ICategoryService categoryService;

	@GetMapping("/api/admin/category")
	public List<CategoryEntity> getAll(@RequestBody CategoryEntity categoryEntity) {
		return categoryService.findAll();
	}

	// Get 1
//	@GetMapping("/api/admin/category/{id}")
//	public ResponseEntity<ResponseObjectDTO> getOneCategory(@RequestBody CategoryEntity categoryEntity,
//			@PathVariable long id) {
//		CategoryEntity cateFind = categoryService.findById(id);
////			System.out.println(cateFind);
////			if(cateFind == null) {
////				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
////						new ResponseObjectDTO("Fail", "Khong tim thay id: "+id, "")
////						);
////			}else {
//		return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("200", "Tim thanh cong", cateFind));
//	}
	// }
	@GetMapping("/api/admin/category/{id}")
	public ResponseEntity<ResponseObjectDTO> getById(@PathVariable long id) {
		try {
			CategoryEntity cateFind = categoryService.findById(id);
			//return new ResponseEntity<CategoryEntity>(cateFind, HttpStatus.OK);
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("200", "Tim thanh cong", cateFind));
		} catch (Exception e) {
//			return new ResponseEntity(HttpStatus.NOT_FOUND);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
				new ResponseObjectDTO("Fail", "Khong tim thay id: "+id, "")
				);
		}
	}
	//PARAMETER
//	@GetMapping("/api/admin/category")
//	public CategoryEntity getOne(@RequestBody CategoryEntity categoryEntity, @RequestParam long id) {
//		return categoryService.findById(id);
//	}
	
	@PostMapping("/api/admin/category")
	public ResponseEntity<ResponseObjectDTO> saveCategory1(@RequestBody CategoryEntity categoryEntity){
		String name = categoryEntity.getName();
		if(name == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					new ResponseObjectDTO("Fail","Ten khong duoc de trong","")
					);
		}
		String description = categoryEntity.getDescription();
		if(description == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					new ResponseObjectDTO("Fail","Mo ta duoc de trong","")
					);
		}
		return ResponseEntity.status(HttpStatus.OK).body(
				new ResponseObjectDTO("200","Insert succesfully",categoryService.saveCategory(categoryEntity))
				);
	}
	
	@PutMapping("/api/admin/category/{id}")
	public ResponseEntity<ResponseObjectDTO> updateCategory(@RequestBody CategoryEntity categoryEntity,@PathVariable long id){
		try {
			CategoryEntity oldCate = categoryService.findById(id);
			if(categoryEntity.getName() == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
						new ResponseObjectDTO("Fail","Ten khong duoc de trong","")
						);
			}
			if(categoryEntity.getDescription() == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
						new ResponseObjectDTO("Fail","Mo ta duoc de trong","")
						);
			}
			oldCate.setName(categoryEntity.getName());
			oldCate.setDescription(categoryEntity.getDescription());
			return ResponseEntity.status(HttpStatus.OK).body(
					new ResponseObjectDTO("200","Sua thanh cong",oldCate)
					);
		}catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					new ResponseObjectDTO("fail","Khong tim thay id: "+id,"")
					);
		}
	}
	
	@DeleteMapping("/api/admin/category/{id}")
	public ResponseEntity<ResponseObjectDTO> deleteCategory(@RequestBody CategoryEntity categoryEntity,@PathVariable long id){
		try {
			categoryService.deleteCategory(id);
			return ResponseEntity.status(HttpStatus.OK).body(
					new ResponseObjectDTO("200","Xoa thanh cong","")
					);
		}catch(Exception e) {
			return ResponseEntity.status(HttpStatus.OK).body(
					new ResponseObjectDTO("Failed","Khong tim thay id: "+id,"")
					);
		}
	}
//	
}
