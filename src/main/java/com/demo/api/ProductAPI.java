package com.demo.api;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.dto.ResponseObjectDTO;
import com.demo.entity.CategoryEntity;
import com.demo.entity.ProductEntity;
import com.demo.service.ICategoryService;
import com.demo.service.IProductService;

@RestController
public class ProductAPI {
	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	@GetMapping("/api/admin/product")
	public List<ProductEntity> findAll() {
		return productService.findAll();
	}

	@GetMapping("/api/admin/product/{id}")
	public ResponseEntity<ResponseObjectDTO> getOneProduct(@RequestBody ProductEntity productEntity,
			@PathVariable long id) {
		try {
			ProductEntity proFound = productService.findOneProduct(id);
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("200", "Tim thanh cong", proFound));
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("fail", "Khong co san pham voi id: " + id, ""));
		}
	}
	@GetMapping("/api/admin/findProduct")
	public ResponseEntity<List<ProductEntity>> listProductByName(
			@RequestParam String name) {
			List<ProductEntity> proFound = productService.findByTitle(name.trim());
			return ResponseEntity.ok(proFound);
		
	}

	@PostMapping("/api/admin/product")
	public ResponseEntity<ResponseObjectDTO> saveProduct(@RequestBody ProductEntity productEntity) {
		// ProductEntity proFound = productService.findOneProduct(id);
		try {
//			CategoryEntity cateFound = categoryService.findById(productEntity.getCategories().getId());
			String name = productEntity.getTitle();
			if (name == null || name == "") {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new ResponseObjectDTO("Fail", "Tieu de khong duoc de trong", ""));
			}
			String detail = productEntity.getDetails();
			if (detail == null || detail == "") {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new ResponseObjectDTO("Fail", "Detail khong duoc de trong", ""));
			}
			BigDecimal price = productEntity.getPrice();
			if (price == null) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND)
						.body(new ResponseObjectDTO("Fail", "Price khong duoc de trong", ""));
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("200", "Them thanh cong", productService.saveProduct(productEntity)));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("fail",
					"Khong co danh muc san pham voi id: " + productEntity.getCategories().getId(), ""));
		}

	}

	@PutMapping("/api/admin/product/{id}")
	public ResponseEntity<ResponseObjectDTO> editProduct(@RequestBody ProductEntity productEntity,
			@PathVariable long id) {
		try {
			ProductEntity proFound = productService.findOneProduct(id);
			proFound.setTitle(productEntity.getTitle());
			proFound.setPrice(productEntity.getPrice());
			proFound.setDetails(productEntity.getDetails());
	//		CategoryEntity cate = categoryService.findById(proFound.getCategories().getId());
			CategoryEntity cate = new CategoryEntity();
			
			try {
				categoryService.findById(productEntity.getCategories().getId());
				cate.setId(productEntity.getCategories().getId());
				proFound.setCategories(cate);
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("fail",
						"Khong co danh muc san pham voi id: " + productEntity.getCategories().getId(), ""));
			}
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("200", "Sua thanh cong ", productService.editProduct(proFound,id)));

		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("fail", "Khong co SAN PHAM voi id: " + id, ""));
		}
	}

	@DeleteMapping("/api/admin/product/{id}")
	public ResponseEntity<ResponseObjectDTO> deleteProduct(@RequestBody ProductEntity productEntity,
			@PathVariable long id) {
		try {
			productService.deleteProduct(id);
			return ResponseEntity.status(HttpStatus.OK).body(new ResponseObjectDTO("200", "Xoa thanh cong", ""));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ResponseObjectDTO("Failed", "Khong tim thay id: " + id, ""));
		}
	}

}
