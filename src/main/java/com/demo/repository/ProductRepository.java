package com.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.entity.ProductEntity;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long>{
	@Query(value = "SELECT e.* FROM tbl_product e Where e.title like %?1%", nativeQuery = true)
	public List<ProductEntity> findByTitle(String title);
}
