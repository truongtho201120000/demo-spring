<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!-- directive cua JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>ListProduct - SB Admin</title>
        
        <!-- VARIABLES -->
		<jsp:include page="/WEB-INF/views/common/variables.jsp"></jsp:include>
		
		<!-- CSS -->
		<jsp:include page="/WEB-INF/views/admin/layout/css.jsp"></jsp:include>
    </head>
    <body class="sb-nav-fixed">
        <!-- HEADER -->
        <jsp:include page="/WEB-INF/views/admin/layout/header.jsp"></jsp:include>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <!-- MENU -->
                <jsp:include page="/WEB-INF/views/admin/layout/menu.jsp"></jsp:include>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                       <!--  <h1 class="mt-4">Product</h1> -->
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item"><a href="${base}/admin/index">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product / List-Product</li>
                        </ol>
                        
                        <h3 align="center">Danh sách product</h3>
                <form>
                      	<label>Tìm kiếm sản phẩm: </label>
                        <input type="text" name="name" id="name"/> <!-- Keyword lấy theo parameter  -->
                        <button type="submit" class="btn btn-info" id="btn-submit" >Search</button>
                        
                  </form>
                  	<div id="getResultDiv" style="padding: 20px 10px 20px 50px">
						
					</div>
                        <form>
                        <a href="${base }/admin/add-product" class="btn btn-primary">Add New Product</a>
                        <p>
                        	<!-- Để dùng đc jstl thì phải include nó vào -->
                        	<table class="table table-striped table-dark">
								  <thead>
								    <tr>
								      <th scope="col">ID</th>
								      <th scope="col">Title</th>
								      <th scope="col">Price</th>
								      <th scope="col">PriceSale</th>
								      <th scope="col">ShortDescription</th>
								      <!-- <th scope="col">Detail</th> -->
								      <th scope="col">Avatar</th>
								      <th scope="col">CategoryName</th>
								      <th scope="col">Action</th>
								    </tr>
								  </thead>
								  <c:forEach var="product" items="${listProduct }">
                        			<!-- items ở đây là biến mà ta đẩy từ thằng controller -->
                        		<%-- <p>${category.name }</p>
                        		<p>${category.description }</p> --%>
                        	
								  <tbody id="load_data">
								    <%-- <tr id="duLieuBanDau">
								      <th scope="row">${product.id }</th>
								      <td>${product.title }</td>
								      <td>${product.price }</td>
								      <td>${product.priceSale }</td>
								      <td>${product.shortDescription }</td>
								      <td>${product.details }</td>
								      <td>
								      		<img alt="" width="50px" src="${base }/upload/${product.avatar}" />
								      </td>
								      <td>${product.categories.name }</td>
								      <td>
								      	
								      	<a href="${base }/admin/delete/${product.seo}" class="btn btn-danger">Delete</a>
								      	<a href="${base }/admin/edit-product/${product.seo }" class="btn btn-success">Edit</a>
								      </td>
								    </tr> --%>
								    
								  </tbody>
								  </c:forEach>
								</table>
                        		<nav aria-label="Page navigation example">
								  <ul class="pagination">
								    <li class="page-item"><a class="page-link" href="${base }/admin/list-product?page=${p-1 }">Previous</a></li>
								    <c:forEach items="${listPage}" var="p">
								    	  <li class="page-item"><a class="page-link" href="${base }/admin/list-product?page=${p }" >${p }</a></li>
								    </c:forEach>
								   
								    <li class="page-item"><a class="page-link" href="#">Next</a></li>
								  </ul>
								</nav>
                        </p>
                        </form>
                    </div>
                </main>
                <!-- Footer -->
                <jsp:include page="/WEB-INF/views/admin/layout/footer.jsp"></jsp:include>
            </div>
        </div>
        <!-- JAVASCRIPT -->
        <jsp:include page="/WEB-INF/views/admin/layout/js.jsp"></jsp:include>
       
</body>
<script>
        $(document).ready(function() {
            //dòng này minh dùng để load data
            LoadData();
        });

        //hàm để load data
        // ở đây mình có sử dụng dùng công chuỗi các tr khi chạy qua từng bản ghi
        // 
        function LoadData() {
            $.ajax({
                url: '/api/admin/product',
                type: 'GET',
                success: function(rs) {
                    var str = "";
                    $.each(rs,function(i, product) {
                    	str +="<tr>";
						str +="<td>"+product.id + "</td>";
						str +="<td>"+product.title + "</td>";
						str +="<td>"+product.price + "</td>";
						str +="<td>"+product.priceSale + "</td>";
						str +="<td>"+product.shortDescription + "</td>";
						str +="<td>"+product.avatar + "</td>";
						str +="<td>"+product.categories.name + "</td>";
						/* str +="<td>"	
				      		<>Delete</a href="${base }/admin/edit-product/${product.seo }">
				      		<a href="${base }/admin/edit-product/${product.seo }" class="btn btn-success">Edit</a>
				      		"</td>"; */
				      	str += "<td><a class='btn btn-success' href='${base}/admin/edit-product/"+product.id+"'>Edit</a><a class='btn btn-danger' href='${base}/admin/delete-product/"+product.id+"'>Delete</a></td>";
						str +="</tr>";
								/* $("#getResultDiv").append(
										product.title,product.price); */
							});
                    // id của thẻ tbody để hiển thị tất cả cá tr ra
                    $('#load_data').html(str);
                }
            });
        }
    </script>  
<script type="text/javascript">
	 $(document).ready(
		function() {
			// GET REQUEST
			$("#btn-submit").click(function(event) {
				event.preventDefault();
				getProductByTitle();
			}); 

			// DO GET
			function getProductByTitle() {
				var ten = $("#name").val();
				/* alert(ten); */
				$.ajax({
					type : "GET",
					url : "/api/admin/findProduct?name="+ten,
					success : function(result) {
					/* 	alert("success"); */
						var str = "";
						$.each(result, function(i, product) {
									str +="<tr>";
									str +="<td>"+product.id + "</td>";
									str +="<td>"+product.title + "</td>";
									str +="<td>"+product.price + "</td>";
									str +="<td>"+product.priceSale + "</td>";
									str +="<td>"+product.shortDescription + "</td>";
									str +="<td>"+product.avatar + "</td>";
									str +="<td>"+product.categories.name + "</td>";
									str +="<td><a class='btn btn-success' href='${base}/admin/edit-product/"+product.id+"'>Edit</a><a class='btn btn-danger' href='${base}/admin/delete-product/"+product.id+"'>Delete</a></td>";
									str +="</tr>";
									/* $("#getResultDiv").append(
											product.title,product.price); */
								});
						$('#load_data').html(str);
						/* $("#duLieuBanDau").css("display", "none"); */
						/* console.log("Success: ", result); */
					},
					error : function(e) {
						console.log("ERROR: ", e);
					}
				});
			}
		 }) 
</script>
<!--     <script type="text/javascript">
    $(document).ready(
    		function() {
    	$("#btn-submit").click(function(event) {
			event.preventDefault();
			getProductByTitle();
		});
    
    	function getProductByTitle() {
			var ten = $("#name").val();
			alert(ten);
			$.ajax({
				type : "GET",
				url : "/api/admin/findProduct?name="+ten,
				success: function(result){
					console.log(1);
					},
				error: function(){
					console.log(2);
				}
			});	
		}})
    </script> -->
</html>
	